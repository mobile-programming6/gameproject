import 'dart:async';
import 'dart:io';
import 'Game.dart';
import 'tableMap.dart';

void main() {
  printStart();
  var enter = stdin.readLineSync()!;
  scheduleTimeout(2 * 1000);
}

void printStart() {
  print("What's your name?");
  stdout.write("Please input your name: ");
  String name = stdin.readLineSync()!;

  print("\nNice to meet you $name!!\n");

  print("¸ .  ₊  ★　° :.　 . • ° 　 .　 *　:.　☆");
  print("° • Welcome to the memory test game • °");
  print("¸ .  ₊  ★　° :.　 . • ° 　 .　 *　:.　☆");

  print("\n══════════════════════════════════════════\n");
  print("How to play\n");
  print(
      "✿———————————————————————————————————————————————————————————————————————————————————————————————————————————✿");

  print(
      "- At the start of the game there will be the first map where we remember the location of the trees and bombs");
  print(
      "- If you walk to the location of the tree, you will not be able to walk");
  print(
      "- If you walk to the location of the bomb, you will lose and exit the game");
  print(
      "- If you walk to collect the flag, you will win. and go to the next checkpoint ");
  print("- The game has 3 levels together");
  print(
      "✿———————————————————————————————————————————————————————————————————————————————————————————————————————————✿\n");

  print("""x = Player
B = Bomb
T = Tree
F = Flag\n""");

  print("✧┈┈┈┈┈┈┈┈┈┈┈┈┈✧");
  print(" are you ready?");
  print("✧┈┈┈┈┈┈┈┈┈┈┈┈┈✧");

  print(" If you ready press Enter to continue..");
}

Timer scheduleTimeout([int milliseconds = 10000]) =>
    Timer(Duration(milliseconds: milliseconds), handleTimeout);

void handleTimeout() {
  clearScreen();

  print("❀┈┈ Level 1 ┈┈❀");
  print("• gives you 3 seconds to remember");
  print("\nPress Enter to show the map and start timing");
  var enter = stdin.readLineSync()!;
  timeoutforShowMap(2 * 1000);
}

Timer timeoutforShowMap([int milliseconds = 10000]) =>
    Timer(Duration(milliseconds: milliseconds), showMap);
void showMap() {
  Game game = new Game();
  game.setLevel(1);
}

void clearScreen() {
  print("\x1B[2J\x1B[0;0H");
}
