import 'dart:io';

import 'Game.dart';
import 'Obj.dart';
import 'tableMap.dart';

class Player extends Obj {
  late tableMap map;
  late Game game = Game();
  late int level = 1;

  Player(int x, int y, String symbol, tableMap map) : super(symbol, x, y) {
    this.map = map;
  }

  bool walk(String direction) {
    switch (direction) {
      case 'N':
      case 'w':
        if (walkN()) return false;
        break;
      case 'S':
      case 's':
        if (walkS()) return false;
        break;
      case 'E':
      case 'd':
        if (walkE()) return false;

        break;
      case 'W':
      case 'a':
        if (walkW()) return false;
        break;
      default:
        return false;
    }

    // checkFlag();
    return true;
  }

  void checkBombLevel1() {
    if (map.isBomb(x, y) || (x == 0 && y == 2) || (x == 2 && y == 0)) {
      print("Founded Bomb!!!! ($x $y)");
      print("• • • • • • • •");
      print("•  YOU LOSE ! •");
      print("• • • • • • • •\n");
      exit(0);
    }
  }

  void checkBombLevel2() {
    if (map.isBomb(x, y) ||
        (x == 0 && y == 0) ||
        (x == 1 && y == 2) ||
        (x == 2 && y == 2)) {
      print("Founded Bomb!!!! ($x $y)");
      print("• • • • • • • •");
      print("•  YOU LOSE ! •");
      print("• • • • • • • •\n");
      exit(0);
    }
  }

  void checkBombLevel3() {
    if (map.isBomb(x, y) ||
        (x == 3 && y == 0) ||
        (x == 0 && y == 1) ||
        (x == 1 && y == 1) ||
        (x == 3 && y == 2)) {
      print("Founded Bomb!!!! ($x $y)");
      print("• • • • • • • •");
      print("•  YOU LOSE ! •");
      print("• • • • • • • •\n");

      exit(0);
    }
  }

  int checkFlag(int level) {
    if (map.isFlag(x, y)) {
      print("Founded Flag!!!! ($x $y)\n");
      level++;
      game.setLevel(level);
      if (level > 3) {
        print(" ✯¸.•´*¨`*•✿ ✿•*`¨*`•.¸✯");
        print("▄▀▄▀▄▀▄▀!! YOU WIN !!▄▀▄▀▄▀▄");
        print(" ✯¸.•´*¨`*•✿ ✿•*`¨*`•.¸✯\n");
        exit(0);
      }
    }
    return level;
  }

  bool walkN() {
    if (canWalk(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (canWalk(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (canWalk(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkW() {
    if (canWalk(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    return false;
  }

  bool canWalk(int x, int y) {
    return map.inMap(x, y) && !map.isTree(x, y);
  }
}
