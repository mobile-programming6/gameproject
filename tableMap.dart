import 'dart:io';
import 'Bomb.dart';
import 'Flag.dart';
import 'Obj.dart';
import 'Player.dart';
import 'Tree.dart';

class tableMap {
  late int width;
  late int height;
  late Player player;
  late Bomb bomb;
  late Tree tree;
  late Flag flag;

  late int count = 0;
  List<Obj> Object = [
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0),
    Obj("", 0, 0)
  ];

  tableMap(int width, int height) {
    this.width = width;
    this.height = height;
  }

  void addObj(Obj obj) {
    Object[count] = obj;
    count++;
  }

  set obj(Obj obj) {
    this.obj = obj;
    addObj(obj);
  }

  void setPlayer(Player player) {
    this.player = player;
    addObj(player);
  }

  void setBomb(Bomb bomb) {
    this.bomb = bomb;
    addObj(bomb);
  }

  void setFlag(Flag flag) {
    this.flag = flag;
    addObj(flag);
  }

  void setTree(Tree tree) {
    this.tree = tree;
    addObj(tree);
  }

  void printSymbolOn(int x, int y) {
    String symbol = '-';
    for (int o = 0; o < count; o++) {
      if (Object[o].isOn(x, y)) {
        symbol = Object[o].getSymbol();
      }
    }
    stdout.write(symbol);
  }

  void showMap() {
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        printSymbolOn(x, y);
        stdout.write(" ");
      }
      print("\n");
    }
  }

  bool inMap(int x, int y) {
    // x -> 0-(width-1), y -> 0-(height-1)
    return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  bool isBomb(int x, int y) {
    return bomb.isOn(x, y);
  }

  bool isFlag(int x, int y) {
    return flag.isOn(x, y);
  }

  bool isTree(int x, int y) {
    for (int o = 0; o < count; o++) {
      if (Object[o] is Tree && Object[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }
}
