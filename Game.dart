import 'dart:async';
import 'dart:io';
import 'Bomb.dart';
import 'Flag.dart';
import 'Tree.dart';
import 'Player.dart';
import 'tableMap.dart';

class Game {
  String direction = "";
  //Player player;
  late tableMap map;
  late int level;

  Game();

  void setLevel(int level) {
    if (level == 1) {
      showMap1();
      //นับถอยหลัง
      scheduleTimeout(3 * 1000);

      //  level = mapPlayLevel1(level);
      // setLevel(level);

    } else if (level == 2) {
      clearLevel1(1 * 1000);

      //  level = mapPlayLevel1(level);
      // setLevel(level);

    } else if (level == 3) {
      clearLevel2(1 * 1000);
      //นับถอยหลัง
      //clear terminal

      //  level = mapPlayLevel1(level);
      // setLevel(level);

    }
  }

  Timer scheduleTimeout([int milliseconds = 10000]) =>
      Timer(Duration(milliseconds: milliseconds), handleTimeout);

  void handleTimeout() {
    clearScreen();
    print("⚠⚠ !!out of time!! ⚠⚠");
    print("•°• walk to the flag •°•\n");
    level = mapPlayLevel1(1);
    // setLevel(level);
  }

  Timer clearLevel1([int milliseconds = 10000]) =>
      Timer(Duration(milliseconds: milliseconds), timeswitLevel2);

  void timeswitLevel2() {
    clearScreen();
    print("╔══════ ❀•°❀°•❀ ══════╗");
    print("       NEXT  LEVEL      ");
    print("╚══════ ❀•°❀°•❀ ══════╝\n");

    print("❀┈┈ Level 2 ┈┈❀");
    print("• gives you 5 seconds to remember");
    print("\nPress Enter to show the map and start timing");
    var enter = stdin.readLineSync()!;
    timeoutforShowMap2(3 * 1000);
  }

  Timer clearLevel2([int milliseconds = 10000]) =>
      Timer(Duration(milliseconds: milliseconds), timeswitLevel3);

  void timeswitLevel3() {
    clearScreen();
    print("╔══════ ❀•°❀°•❀ ══════╗");
    print("       NEXT  LEVEL      ");
    print("╚══════ ❀•°❀°•❀ ══════╝\n");

    print("❀┈┈ Level 3 ┈┈❀");
    print("• gives you 10 seconds to remember");
    print("\nPress Enter to show the map and start timing");
    var enter = stdin.readLineSync()!;
    timeoutforShowMap3(3 * 1000);
  }

  Timer timeoutforShowMap3([int milliseconds = 10000]) =>
      Timer(Duration(milliseconds: milliseconds), timeMapPlay3);
  void timeMapPlay3() {
    showMap3();
    //นับถอยหลัง //clear terminal
    scheduleTimeout3(8 * 1000);
  }

  Timer timeoutforShowMap2([int milliseconds = 10000]) =>
      Timer(Duration(milliseconds: milliseconds), timeMapPlay2);
  void timeMapPlay2() {
    showMap2();
    //นับถอยหลัง //clear terminal
    scheduleTimeout2(5 * 1000);
  }

  Timer scheduleTimeout2([int milliseconds = 10000]) =>
      Timer(Duration(milliseconds: milliseconds), handleTimeout2);

  void handleTimeout2() {
    clearScreen();
    print("⚠⚠ !!out of time!! ⚠⚠");
    print("•°• walk to the flag •°•\n");
    level = mapPlayLevel2(2);
    // setLevel(level);
  }

  Timer scheduleTimeout3([int milliseconds = 10000]) =>
      Timer(Duration(milliseconds: milliseconds), handleTimeout3);

  void handleTimeout3() {
    clearScreen();
    print("⚠⚠ !!out of time!! ⚠⚠");
    print("•°• walk to the flag •°•\n");
    level = mapPlayLevel3(3);
    // setLevel(level);
  }

  void clearScreen() {
    print("\x1B[2J\x1B[0;0H");
  }

  String inputDirection() {
    direction = stdin.readLineSync()!;
    print("");
    return direction;
  }

  void showMap1() {
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (i == 1 && j == 0) {
          stdout.write('T ');
        } else if (i == 0 && j == 2) {
          stdout.write('B ');
        } else if (i == 2 && j == 0) {
          stdout.write('B ');
        } else {
          stdout.write('- ');
        }
      }
      print("\n");
    }
  }

  void showMap2() {
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 4; j++) {
        if (i == 0 && j == 0) {
          stdout.write('B ');
        } else if (i == 1 && j == 2) {
          stdout.write('T ');
        } else if (i == 2 && j == 1) {
          stdout.write('B ');
        } else if (i == 2 && j == 2) {
          stdout.write('B ');
        } else {
          stdout.write('- ');
        }
      }
      print("\n");
    }
  }

  void showMap3() {
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 5; j++) {
        if (i == 1 && j == 1) {
          stdout.write('B ');
        } else if (i == 1 && j == 0) {
          stdout.write('B ');
        } else if (i == 0 && j == 3) {
          stdout.write('B ');
        } else if (i == 2 && j == 3) {
          stdout.write('B ');
        } else if (i == 1 && j == 4) {
          stdout.write('T ');
        } else {
          stdout.write('- ');
        }
      }
      print("\n");
    }
  }

  int mapPlayLevel1(int level) {
    tableMap map = new tableMap(3, 3);
    Player player = new Player(0, 0, 'x', map);
    Bomb bomb1 = new Bomb(0, 2);
    Bomb bomb2 = new Bomb(2, 0);
    Tree tree = new Tree(0, 1);
    Flag flag = new Flag(2, 2);
    map.setPlayer(player);
    map.setBomb(bomb1);
    map.setBomb(bomb2);
    map.setTree(tree);
    map.setFlag(flag);

    while (true) {
      map.showMap();
      // W,a| N,w| E,d| S,s
      // B: gameOver
      // F: Win
      stdout.write("Please input direction: ");
      inputDirection();
      player.walk(direction);
      player.checkBombLevel1();
      level = player.checkFlag(level);
      if (level > 1) {
        return level;
      }
    }
  }

  int mapPlayLevel2(int level) {
    tableMap map = new tableMap(4, 3);
    Player player = new Player(2, 0, 'x', map);
    Bomb bomb1 = new Bomb(0, 0);
    Bomb bomb2 = new Bomb(1, 2);
    Bomb bomb3 = new Bomb(2, 2);
    Tree tree = new Tree(2, 1);
    Flag flag = new Flag(0, 2);
    map.setPlayer(player);
    map.setBomb(bomb1);
    map.setBomb(bomb2);
    map.setBomb(bomb3);
    map.setTree(tree);
    map.setFlag(flag);

    while (true) {
      map.showMap();
      // W,a| N,w| E,d| S,s
      // B: gameOver
      // F: Win
      stdout.write("Please input direction: ");
      inputDirection();
      player.walk(direction);
      player.checkBombLevel2();
      level = player.checkFlag(level);
      if (level > 2) {
        return level;
      }
    }
  }

  int mapPlayLevel3(int level) {
    tableMap map = new tableMap(5, 3);
    Player player = new Player(0, 0, 'x', map);
    Bomb bomb1 = new Bomb(3, 0);
    Bomb bomb2 = new Bomb(0, 1);
    Bomb bomb3 = new Bomb(1, 1);
    Bomb bomb4 = new Bomb(3, 2);
    Tree tree = new Tree(4, 1);
    Flag flag = new Flag(0, 2);
    map.setPlayer(player);
    map.setBomb(bomb1);
    map.setBomb(bomb2);
    map.setBomb(bomb3);
    map.setBomb(bomb4);
    map.setTree(tree);
    map.setFlag(flag);

    while (true) {
      map.showMap();
      // W,a| N,w| E,d| S,s
      // B: gameOver
      // F: Win
      stdout.write("Please input direction: ");
      inputDirection();
      player.walk(direction);
      player.checkBombLevel3();
      level = player.checkFlag(level);
      if (level > 3) {
        return level;
      }
    }
  }
}
